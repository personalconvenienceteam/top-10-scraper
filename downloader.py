#-----------------------------------------------------------
#
# Web Page Downloader
#
# For a particular URL, this simple program downloads a web
# page and prints it to the Python shell window.
# You can then examine the HTML/XML source of the document
# and copy parts of it into the "regex tester" to help you
# develop your expressions for extracting particular document
# elements.  This simple script has no user interface or error
# handling, but feel free to add them if you want!
#
# Q: Why not just look at the web page's source in your
# favourite web browser (Firefox, Google Chrome, etc)?
#
# A: Because when a Python script uses the hyper-text transfer
# protocol to download a web document, it may not receive
# the same file you see in your browser!  Some web servers
# produce different HTML or XML code for different browsers.
# Therefore, to guarantee that the code you inspect is the
# same code that your own Python program sees, it's safer
# to download the web page using this script.
#

from urllib import urlopen
from re import findall
from Tkinter import *
url = 'http://www.silverkgallery.com.au/Heroes%20N%20Villains/Marvel%20Canvas/MarvelComics.gif' # Put your web page address here

splash_window=Tk()

def gif_to_PhotoImage(gif_image):

    # Encode the byte stream as a base-64 character string
    # (MIME Base 64 format)
    characters = gif_image.encode('base64', 'strict')

    # Return the result as a Tkinter PhotoImage
    return PhotoImage(data = characters)

test_image_data=urlopen(url).read()
test_image=gif_to_PhotoImage(test_image_data)
print test_image

label=Label(splash_window, image=test_image).pack()
splash_window.mainloop()

# Read the contents of the web page as a string

# At this point you have the Web document as a string,
# so you can put whatever Python code you like here
# to manipulate it, or cut-and-paste the displayed contents
# from IDLE's shell window into some other tool such as
# the "regex tester" or a text editor


