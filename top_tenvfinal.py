
#-----Statement of Authorship----------------------------------------#
#
#  This is an individual assessment item.  By submitting this
#  code I agree that it represents my own work.  I am aware of
#  the University rule that a student must not act in a manner
#  which constitutes academic dishonesty as stated and explained
#  in QUT's Manual of Policies and Procedures, Section C/5.3
#  "Academic Integrity" and Section E/2.1 "Student Code of Conduct".
#
#    Student no: N9438157
#    Student name: Roderick Lenz
#
#  NB: Files submitted without a completed copy of this statement
#  will not be marked.  Submitted files may be subjected to
#  software plagiarism analysis using the MoSS system
#  (http://theory.stanford.edu/~aiken/moss/).
#
#--------------------------------------------------------------------#



#-----Task Description-----------------------------------------------#
#
#  The Top Ten of Everything 
#
#  In this task you will combine your knowledge of HTMl/XML mark-up
#  languages with your skills in Python scripting, pattern matching
#  and Graphical User Interface design to produce a useful
#  application for accessing online data.  See the instruction
#  sheet accompanying this template for full details.
#
#--------------------------------------------------------------------#



#--------------------------------------------------------------------#
#
#  Import the modules needed for this assignment.  You may not import
#  any other modules or rely on any other files.  All data and images
#  needed for your solution must be sourced from the Internet.
#

# Import the function for downloading web pages
from urllib import urlopen

# Import the regular expression function
from re import findall

# Import the Tkinter functions
from Tkinter import *

# Import Python's HTML parser
from HTMLParser import *

#Import SQL functions
from sqlite3 import *


#--------------------------------------------------------------------#
#
#  Utility function:
#  Given the raw byte stream of a GIF image, return a Tkinter
#  PhotoImage object suitable for use as the 'image' attribute
#  in a Tkinter Label widget or any other such widget that
#  can display images.
#
def gif_to_PhotoImage(gif_image):

    # Encode the byte stream as a base-64 character string
    # (MIME Base 64 format)
    characters = gif_image.encode('base64', 'strict')

    # Return the result as a Tkinter PhotoImage
    return PhotoImage(data = characters)



#--------------------------------------------------------------------#
#
#  Utility function:
#  Given the raw byte stream of a JPG or PNG image, return a
#  Tkinter PhotoImage object suitable for use as the 'image'
#  attribute in a Tkinter Label widget or any other such widget
#  that can display images.  If positive integers are supplied for
#  the width and height (in pixels) the image will be resized
#  accordingly.
#
def image_to_PhotoImage(image, width = None, height = None):

    # Import the Python Imaging Library, if it exists
    try:
        from PIL import Image, ImageTk
    except:
        raise Exception, 'Python Imaging Library has not been installed properly!'

    # Import StringIO for character conversions
    from StringIO import StringIO

    # Convert the raw bytes into characters
    image_chars = StringIO(image)

    # Open the character string as a PIL image
    pil_image = Image.open(image_chars)
    
    # Resize the image, if a new size has been provided
    if type(width) == int and type(height) == int and width > 0 and height > 0:
        pil_image = pil_image.resize((width, height), Image.ANTIALIAS)

    # Return the result as a Tkinter PhotoImage
    return ImageTk.PhotoImage(pil_image)



#-----Student's Solution---------------------------------------------#
#
#  Complete the assignment by putting your solution below.
#


##### DEVELOP YOUR SOLUTION HERE #####

#Create and configure the main window
splash_window = Tk()
splash_window.title('The Greatest Collection of Top Ten Lists Ever')

#Create lists for urls and regular expressions to be passed to functions later
button_list=[]
splash_images=[]
splash_image_urls=['http://1.bp.blogspot.com/-e_I31T3Z65s/UzaMhvQosQI/AAAAAAAA-zU/TIcoNsX1Ses/s1600/top-ten.gif',
                   'http://www.silverkgallery.com.au/Heroes%20N%20Villains/Marvel%20Canvas/MarvelComics.gif',
                   'https://upload.wikimedia.org/wikipedia/commons/7/73/Pentagram.gif',
                   'https://media.giphy.com/media/UgdsbZzb2MIjS/giphy.gif']

website_url_list=['http://www.ranker.com/crowdranked-list/top-marvel-comics-superheroes?var=6&utm_expid=16418821-201.EEIZkBszS3O1rZiBcoCRjg.3&format=BLOG&utm_referrer=http%3A%2F%2Fwww.ranker.com%2Fcrowdranked-list%2Ftop-marvel-comics-superheroes%3Fvar%3D6%26format%3DBLOG',
                   'http://www.billboard.com/charts/hard-rock-albums',
                   'http://store.steampowered.com/stats/']

regex_list=['itemprop=["]name["]>([^/]+)</span',
            'chart-row__song["]>([^<]+)</h2>',
            'onmouseover=[^>]+>([^<]+)</a>']

#The following downloads, converts and stores the images used in the views
for image in splash_image_urls:
    splash_image_data=urlopen(image).read()
    splash_image=gif_to_PhotoImage(splash_image_data)
    splash_images.append(splash_image)

#This function reads the desired website from the website_url_list, uses the matching
#regular expression from the regex_list to create the list of items and tidies them up
def load_website_data(website):
    website_data=urlopen(website_url_list[website]).read()
    data_list=findall(regex_list[website],website_data)
    data_list=[item.strip() for item in data_list]
    data_list=[item.replace("&amp;","&") for item in data_list]
    return data_list

#This function inputs the items into the list and applies the rank
def list_entry(current_list):
    list_box.insert(END, "\n") #this provides and extra line at top of the box for aesthetic reasons
    for item in range (0,10):
        list_box.insert(END, str(item+1)+": "+current_list[item]+"\n")


#The following line create the Tkinter widgets    
splash_label=Label(splash_window)
splash_text=Label(splash_window,font=('Arial',18))
list_box=Text(splash_window, font=('Arial', 18))
list_box.tag_configure('center', justify=CENTER)
url_label=Label(splash_window, font=('Arial', 8))

#This function modifies the GUI window, calling up the requested
#list view when a button is pushed.
def go_to_list(list_number):
    global current_list
    splash_label.config(image=splash_images[list_number+1]) #Calls the image for the selected view
    list_box.configure(state=NORMAL) #Enables the list box for editing
    list_box.delete(1.0,END) #Clears the list box
    current_list = load_website_data(list_number) #Calls, downloads and reads the requested website
    if list_number==0: #Creates the Super Hero view
        button_row=3
        del current_list[0] #The regex returns an initial line that is unwanted, this removes it
        del current_list[10:] #Removes unwanted entries from the end of the list
        splash_text.config(text='Proving Marvel is Better than DC:\n Top 10 Superheros of all time',
                           bg='blue',fg='white')
        splash_window.config(bg='red')
        list_box.configure(width=20,height=12,bg='white',fg='blue')
        splash_label.grid(row=1,columnspan=5)
        splash_text.grid(row=0, column=2, columnspan=3)
        list_box.grid(row=1,column=5,columnspan=5)
        list_entry(current_list) #Calls the list_entry function to fill the list box
        url_label.config(text='http://www.ranker.com/crowdranked-list/top-marvel-comics-superheroes',
                         bg='red',fg='blue')#Adds the url to the view

    if list_number==1: #Creates the Hard Rock chart
        button_row=3
        del current_list[10:] #Removes unwanted entries from the end of the list

        #In order to combine the Album with the artist, this website required a separate regex to
        #find the artist names. The following grabs those names and adds them to the list
        #in the appropriate place.
        website_data=urlopen(website_url_list[list_number]).read()
        artist_list=findall('data-tracklabel=["]Artist.Name["]>([^<]+)</a>[\n]',
                            website_data)
        del artist_list[10:] #Removes unwanted entries from the end of the list
        artist_list=[artist.strip() for artist in artist_list] #Cleans up the list

        #The following combines the artist name and album in the appropriate place
        current_list=[artist_list[album]+" - "+current_list[album] for album in range(len(artist_list))] 

        #Widget configuration
        splash_text.config(text='Rocking out like a beast: Top 10 of Hard Rock',
                           fg='Red',bg='black')
        splash_text.grid(row=0, column=2, columnspan=3)
        splash_window.config(bg='black')
        list_box.configure(width=80,height=12,fg='Red',bg='black')
        splash_label.grid(row=1,columnspan=2)
        list_box.grid(row=1,column=2,columnspan=5,ipadx=5)
        
        list_entry(current_list) #Fills the list box
        list_box.tag_add('center', 1.0, END) #justifies the text in the box
        url_label.config(text='http://www.billboard.com/charts/hard-rock-albums',
                         bg='black',fg='red')#Adds the url to the view

    if list_number==2: #Creates the Games Chart
        button_row=4
        del current_list[10:] #Removes unwanted entries from the end of the list

        #Widget configuration
        splash_text.config(text='Game On!:\nTop 10 Most Played Games on Steam',
                           bg='slate gray',fg='white')
        splash_text.grid(row=0, column=0, columnspan=5)
        splash_window.config(bg='gray21')
        list_box.configure(width=30,height=12,bg='slate gray', fg='white')
        list_box.grid(row=2,column=0,columnspan=5)
        splash_label.grid(row=1,column=0,columnspan=5)
 
        list_entry(current_list) #Fills the list box
        list_box.tag_add('center', 1.0, END) #justifies the text in the box
        url_label.config(text='http://store.steampowered.com/stats/',bg='gray21',
                         fg='light grey')#Adds the url to the view

    #Resets the buttons for the view
    for button in button_list:
        button.grid(row=button_row)
        button['state']=NORMAL
    list_box.configure(state=DISABLED)#Disables text entry in the list box
    button_list[list_number].config(state=DISABLED)#Disables the button for the current list
    home_button.grid(row=button_row, column=0)#Shows the home button
    save_button.grid(row=button_row, column=4)#Shows the save button
    url_label.grid(row=button_row-1, column=0,columnspan=5)#Shows the list data URL source
    
#This function creates the home view
def return_home():
    splash_label.config(image=splash_images[0])
    splash_label.grid(row=1,column=0,columnspan=5)
    splash_text.config(text='The Greatest Top Ten Lists',bg='light gray',fg='black')
    splash_window.config(bg='light gray')
    splash_text.grid(row=0, column=1, columnspan=3)
    #Reset the list buttons
    for button in button_list:
        button.grid(row=2)
        button['state']=NORMAL
    home_button.grid_remove() #Hide the home button
    list_box.grid_remove() #Hide the list box
    url_label.grid_remove() #Hide the URL label
    save_button.grid_remove() #Hide the save button

#This function opens, clears and saves the current list to the database
def save_list():
    connection = connect(database='top_ten.db')
    top_ten_db = connection.cursor()
    top_ten_db.execute("DELETE FROM 'Top_Ten'")
    rank=1
    for item in current_list:
        rank_item=[rank, item]
        top_ten_db.execute("INSERT INTO 'Top_Ten' VALUES(?,?)",rank_item)
        rank+=1
    connection.commit()
    top_ten_db.close()
    connection.close()

#The following creates the buttons and stores them in the button list for
#easier manipulation in the functions
    
button_1=Button(splash_window, text='Super Hero Rankings',command=lambda: go_to_list(0))
button_1.grid(row=2,column=1,sticky=W+E)
button_2=Button(splash_window, text='Hard Rock Album Chart',command=lambda: go_to_list(1))
button_2.grid(row=2,column=2,sticky=W+E)
button_3=Button(splash_window, text='Top Steam Games',command=lambda: go_to_list(2))
button_3.grid(row=2,column=3,sticky=W+E)
home_button = Button(splash_window, text='Home',command=return_home)
save_button = Button(splash_window, text='Save List', command=save_list)
button_list = [button_1, button_2, button_3]

return_home() #shows the home page on startup

splash_window.mainloop()


